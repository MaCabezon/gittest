package tarta;
import java.io.*;

public class Main {
    
    public static void main(String[] args) throws IOException{
        InputStreamReader conv = new InputStreamReader (System.in);
        BufferedReader entrada = new BufferedReader (conv);
        
        String leido;
        Tarta tarta1 = new Tarta();
        
        System.out.println("Bienvenidos al selector de ingredientes:");
        System.out.println("--Lista de intgredientes--");
        for (int c=0; c<4; c++) {
            System.out.println(Tarta.ing[c]);
        }
        System.out.println("Eliga ahora 3 ingredientes de la lista:");
        System.out.println("Ingrediente Nº1:");
        leido=entrada.readLine();
        tarta1.setIngrediente1(leido);
        System.out.println("Ingrediente Nº2:");
        leido=entrada.readLine();
        tarta1.setIngrediente2(leido);
        System.out.println("Ingrediente Nº3:");
        leido=entrada.readLine();
        tarta1.setIngrediente3(leido);
        
        System.out.println("------------------");
        System.out.println("Precio total: "+tarta1.calcularPrecio()+" €.");
    }
    
}
