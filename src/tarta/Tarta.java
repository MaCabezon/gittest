package tarta;

public class Tarta {

    private String nombre;
    private String ingrediente1;
    private String ingrediente2;
    private String ingrediente3;
    /*static String ing[] = new String[3];*/
    static String ing[]={"nata","fresa","chocolate","limon"};
    /*static int precio[] = new int[3];*/
    static int precio[]={3,5,4,2};       

  
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIngrediente1() {
        return ingrediente1;
    }

    public void setIngrediente1(String ingrediente1) {
        this.ingrediente1 = ingrediente1;
    }

    public String getIngrediente2() {
        return ingrediente2;
    }

    public void setIngrediente2(String ingrediente2) {
        this.ingrediente2 = ingrediente2;
    }

    public String getIngrediente3() {
        return ingrediente3;
    }

    public void setIngrediente3(String ingrediente3) {
        this.ingrediente3 = ingrediente3;
    }
    
    public int calcularPrecio(){
        int preciototal;
        int p1=0, p2=0, p3=0;
        
        for (int c=0; c<4; c++) {
            if (ingrediente1.equals(ing[c])) {
                p1=precio[c];
            }
        }
        
        for (int c=0; c<4; c++) {
            if (ingrediente2.equals(ing[c])) {
                p2=precio[c];
            }
        }
        
        for (int c=0; c<4; c++) {
            if (ingrediente3.equals(ing[c])) {
                p3=precio[c];
            }
        }
        
        preciototal=p1+p2+p3;
        return preciototal;
    }

    
}
